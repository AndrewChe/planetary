﻿using UnityEngine;
using System.Collections;

public class esc_menu : MonoBehaviour {

	// Use this for initialization
	public void resume () {
		gameObject.active = false;

		if (Cursor.lockState == CursorLockMode.Confined){
			Cursor.lockState = CursorLockMode.Locked;
		} else {
			Cursor.lockState = CursorLockMode.Confined;
		}

		Cursor.visible = !Cursor.visible;
	}

	public void respawn () {
		
		GameObject SpawnPoint = GameObject.Find ("SpawnPoint");

		if (PhotonNetwork.player.GetTeam () == PunTeams.Team.blue) {
			SpawnPoint = GameObject.Find ("SpawnPoint_blue");
		}

		if (PhotonNetwork.player.GetTeam () == PunTeams.Team.red) {
			SpawnPoint = GameObject.Find ("SpawnPoint_red");
		}

		GameObject[] Players = GameObject.FindGameObjectsWithTag ("Player");

		foreach (GameObject p in Players) {
			if (p.GetComponent<PhotonView> ().isMine) {
				PhotonNetwork.Destroy (p.GetComponent<PhotonView> ());
				PhotonNetwork.Instantiate ("player", SpawnPoint.transform.position, SpawnPoint.transform.rotation, 0);
			}
		}

	}

	public void exit () {
		
		PhotonNetwork.LeaveRoom ();
		Application.LoadLevel (0);
	}
}
