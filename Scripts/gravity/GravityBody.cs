﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent (typeof (Rigidbody))]
public class GravityBody : MonoBehaviour {
	
	public List<GravityAttractor> planets = new List<GravityAttractor>();
	Rigidbody rigidbody;
	
	void Awake () {
		rigidbody = GetComponent<Rigidbody> ();

		// Disable rigidbody gravity and rotation as this is simulated in GravityAttractor script
		rigidbody.useGravity = false;
		rigidbody.constraints = RigidbodyConstraints.FreezeRotation;
	}

	void FixedUpdate () {
		// Allow this body to be influenced by planet's gravity
		for (int i = 0; i < planets.Count; i++) {
			planets[i].Attract (rigidbody);
		}
	}

	void Update () {
		for (int i = 0; i < planets.Count; i++) {

			if (planets [i] == null) {
				planets.Remove (planets [i]);

			}
		}
	}
}