﻿using UnityEngine;
using System.Collections;

public class GravityAttractor : MonoBehaviour {
	
	public enum AttractorTypes
	{
		planetary,
		vertical,
	};



	public AttractorTypes _attractorType = AttractorTypes.planetary;

	public float gravity = -9.8f;

	public Vector3 verticalgravityUp;




	public void Attract(Rigidbody body) {

		if (_attractorType == AttractorTypes.planetary) {

			float mmm = -(gravity / Mathf.Abs (gravity));
			//Debug.Log (mmm);
			Vector3 gravityUp = (body.position - transform.position).normalized;

			Vector3 localUp = body.transform.up;
		
			// Apply downwards gravity to body
			body.AddForce (gravityUp * gravity);
			// Allign bodies up axis with the centre of planet
			if ((body.GetComponent<GravityBody> () && body.GetComponent<GravityBody> ().planets.Count == 1) || (body.GetComponent<GravityRigidbody> () && body.GetComponent<GravityRigidbody> ().planets.Count == 1)) {
				//body.rotation = Quaternion.FromToRotation (localUp, gravityUp) * body.rotation;
				body.rotation = Quaternion.Slerp (this.transform.rotation, Quaternion.FromToRotation (localUp, gravityUp * mmm), 0.1f) * body.rotation;
			}

		}

		// vertical attractor
		if (_attractorType == AttractorTypes.vertical) {
			float mmm = -(gravity / Mathf.Abs (gravity));

			verticalgravityUp = verticalgravityUp.normalized;	
			Vector3 localUp = body.transform.up;
			// Apply downwards gravity to body
			body.AddForce (verticalgravityUp * gravity);
			// Allign bodies up axis with the centre of planet
			if (((body.GetComponent<GravityBody> () && body.GetComponent<GravityBody> ().planets.Count == 1) || (body.GetComponent<GravityRigidbody> () && body.GetComponent<GravityRigidbody> ().planets.Count == 1))) {
				//body.rotation = Quaternion.FromToRotation (localUp, gravityUp) * body.rotation;
				body.rotation = Quaternion.Slerp (this.transform.rotation, Quaternion.FromToRotation (localUp, verticalgravityUp * mmm), 0.1f) * body.rotation;
			}
		}


	}  

	public void AttractRigidbody(Rigidbody body) {

		if (_attractorType == AttractorTypes.planetary) {

			Vector3 gravityUp = (body.position - transform.position).normalized;

			// Apply downwards gravity to body
			body.AddForce (gravityUp * gravity);
		}

		if (_attractorType == AttractorTypes.vertical) {
			Vector3 gravityUp = gameObject.transform.up;

			// Apply downwards gravity to body
			body.AddForce (gravityUp * gravity);
		}


	}  




	void OnTriggerEnter(Collider smth){
		
		if (smth.GetComponent<GravityBody> ()) {
			GravityBody gg = smth.GetComponent<GravityBody> ();
			gg.planets.Add (this.GetComponent<GravityAttractor> ());
		} else {
			if (smth.GetComponent<GravityRigidbody> ()){
				GravityRigidbody gg = smth.GetComponent<GravityRigidbody> ();
				gg.planets.Add(this.GetComponent<GravityAttractor> ());
			}
		}

	}





	void OnTriggerExit(Collider smth){
		if (smth.GetComponent<GravityBody> ()) {
			GravityBody gg = smth.GetComponent<GravityBody> ();
			gg.planets.Remove (this.GetComponent<GravityAttractor> ());
		} else {
			if (smth.GetComponent<GravityRigidbody> ()){
				GravityRigidbody gg = smth.GetComponent<GravityRigidbody> ();
				gg.planets.Remove (this.GetComponent<GravityAttractor> ());
			}
		}
	}
}
