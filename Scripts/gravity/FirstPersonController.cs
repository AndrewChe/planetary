﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof (GravityBody))]
public class FirstPersonController : MonoBehaviour {
	
	// public vars
	public float mouseSensitivityX = 1;
	public float mouseSensitivityY = 1;
	public float walkSpeed = 6;
	public float jumpForce = 220;
	public LayerMask groundedMask;


	/////////////////Andrew's place/////////////////////

	public GameObject tab_panel;
	public GameObject tab_panel_team;
	public GameObject esc_panel;
	public GameObject manager;


	////////////////////////////////////////////////////
	
	// System vars
	public bool grounded;
	Vector3 moveAmount;
	Vector3 smoothMoveVelocity;
	float verticalLookRotation;
	Transform cameraTransform;
	Rigidbody rigidbody;
	
	
	void Awake() {
		Cursor.lockState = CursorLockMode.Locked;
		Cursor.visible = false;
		tab_panel = Instantiate (tab_panel) as GameObject;
		tab_panel_team = Instantiate (tab_panel_team) as GameObject;

		esc_panel = Instantiate (esc_panel) as GameObject;
		tab_panel.active = false;
		esc_panel.active = false;
		tab_panel_team.active = false;
		manager = GameObject.Find ("_gameManager");

		cameraTransform = Camera.main.transform;
		rigidbody = GetComponent<Rigidbody> ();
	}
	
	void Update() {
		
		// Look rotation:

		if (!esc_panel.active) {
		transform.Rotate(Vector3.up * Input.GetAxis("Mouse X") * mouseSensitivityX);
		verticalLookRotation += Input.GetAxis("Mouse Y") * mouseSensitivityY;
		verticalLookRotation = Mathf.Clamp(verticalLookRotation,-60,60);
		cameraTransform.localEulerAngles = Vector3.left * verticalLookRotation;

		}
		// Calculate movement:
		if (grounded) {
			float inputX = Input.GetAxisRaw ("Horizontal");
			float inputY = Input.GetAxisRaw ("Vertical");
		
			Vector3 moveDir = new Vector3 (inputX, 0, inputY).normalized;
			Vector3 targetMoveAmount = moveDir * walkSpeed;
			moveAmount = Vector3.SmoothDamp (moveAmount, targetMoveAmount, ref smoothMoveVelocity, .15f);
		}
		// Jump
		if (Input.GetButtonDown("Jump")) {
			if (grounded) {
				rigidbody.AddForce(transform.up * jumpForce);
			}
		}
		
		// Grounded check
		Ray ray = new Ray(transform.position, -transform.up);
		RaycastHit hit;
		
		if (Physics.Raycast(ray, out hit, 1 + .1f, groundedMask)) {
			grounded = true;
		}
		else {
			grounded = false;
		}

		//tab


		if (manager.GetComponent<GameManager> ().type == Menu.GameType.CaptureTheFlag) {
			
		


			if (Input.GetKey (KeyCode.Tab) && (!esc_panel.active)) {
		
				tab_panel_team.active = true;

			} else {

				tab_panel_team.active = false;

			}
		}

		if (manager.GetComponent<GameManager> ().type == Menu.GameType.Free) {




			if (Input.GetKey (KeyCode.Tab) && (!esc_panel.active)) {

				tab_panel.active = true;

			} else {

				tab_panel.active = false;

			}
		}

		//esc
		if (Input.GetKeyDown (KeyCode.Escape)) {



			esc_panel.active = !esc_panel.active;
			if (Cursor.lockState == CursorLockMode.Confined){
				Cursor.lockState = CursorLockMode.Locked;
			} else {
				Cursor.lockState = CursorLockMode.Confined;
			}

			Cursor.visible = !Cursor.visible;

		}

		if (Input.GetKeyDown (KeyCode.R)) {
			GameObject SpawnPoint = GameObject.Find ("SpawnPoint");

			if (PhotonNetwork.player.GetTeam () == PunTeams.Team.blue) {
				SpawnPoint = GameObject.Find ("SpawnPoint_blue");
			}

			if (PhotonNetwork.player.GetTeam () == PunTeams.Team.red) {
				SpawnPoint = GameObject.Find ("SpawnPoint_red");
			}

			GameObject[] Players = GameObject.FindGameObjectsWithTag ("Player");


			foreach (GameObject p in Players) {
				if (p.GetComponent<PhotonView> ().isMine) {
					PhotonNetwork.Destroy (p.GetComponent<PhotonView> ());
					PhotonNetwork.Instantiate ("player", SpawnPoint.transform.position, SpawnPoint.transform.rotation, 0);
				}
			}
		}
		
	}
	
	void FixedUpdate() {
		// Apply movement to rigidbody
		Vector3 localMove = transform.TransformDirection(moveAmount) * Time.fixedDeltaTime;
		rigidbody.MovePosition(rigidbody.position + localMove);
	}
}
