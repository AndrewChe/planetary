﻿using UnityEngine;
using System.Collections;

public class FlagScript : MonoBehaviour {

	public PunTeams.Team team;

	public Material blue_mat;
	public Material red_mat;

	public GameObject flag;






	void OnTriggerEnter (Collider body) {
		Debug.Log ((bool) body.GetComponent<PhotonView> ().owner.customProperties ["hasFlag"]);
		if (body.GetComponent<PhotonView> ().owner.GetTeam () != team) {
			body.GetComponent<PhotonView> ().owner.SetCustomProperties (new ExitGames.Client.Photon.Hashtable(){{"hasFlag", true}});
		}

		if ((body.GetComponent<PhotonView> ().owner.GetTeam () == team) & ((bool)body.GetComponent<PhotonView> ().owner.customProperties ["hasFlag"])) {
			body.GetComponent<PhotonView> ().owner.SetCustomProperties (new ExitGames.Client.Photon.Hashtable(){{"hasFlag", false}});
			body.GetComponent<PhotonView> ().owner.AddScore (100);

		}
	}



	void Start () {
		if (team == PunTeams.Team.blue) {
			flag.GetComponent<Renderer> ().material = blue_mat;
		}
		if (team == PunTeams.Team.red) {
			flag.GetComponent<Renderer> ().material = red_mat;
		}

	}

	// Update is called once per frame
	void Update () {
	
	}
}
