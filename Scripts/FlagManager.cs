﻿using UnityEngine;
using System.Collections;

public class FlagManager : MonoBehaviour {


	// Use this for initialization
	public GameObject redFlag;
	public GameObject blueFlag;




	void Update () {
		bool showb = true;
		bool showr = true;

		if (((bool) PhotonNetwork.player.customProperties ["hasFlag"]) && (PhotonNetwork.player.GetTeam () != PunTeams.Team.red)) {
			showr = false;
		} else {
			foreach (PhotonPlayer player in PhotonNetwork.otherPlayers) {
				if (((bool)player.customProperties ["hasFlag"]) && (player.GetTeam () != PunTeams.Team.red)) {
					showr = false;
				}

			}
		}
		//
		//

		if (((bool) PhotonNetwork.player.customProperties ["hasFlag"]) && (PhotonNetwork.player.GetTeam () != PunTeams.Team.blue)) {
			showb = false;
		} else {
			foreach (PhotonPlayer player in PhotonNetwork.otherPlayers) {
				if (((bool)player.customProperties ["hasFlag"]) && (player.GetTeam () != PunTeams.Team.blue)) {
					showb = false;
				}

			}
		}

		//
		//
		if (showr) {
			redFlag.active = true;
		} else {
			redFlag.active = false;
		}

		if (showb) {
			blueFlag.active = true;
		} else {
			blueFlag.active = false;
		}
	}

}
