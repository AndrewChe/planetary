﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Hashtable = ExitGames.Client.Photon.Hashtable;


public class Menu : Photon.MonoBehaviour {


	public GameObject mapselector;
	public GameObject _camera;
	private int _state;
	//private string UserName;


	public enum GameType {
		Free,
		FreeForAll,
		CaptureTheFlag,
		Sandbox,
	}


	public GameManager manager;

	void Start () {
		_state = 0;
	}


	public void Login (GameObject name) {
		//PhotonNetwork.autoJoinLobby = false;
		PhotonNetwork.ConnectUsingSettings ("0.6.9.5 alfa");
		manager.UserName = name.GetComponent<Text> ().text;
		Debug.Log ("hello, " + manager.UserName);
		PhotonNetwork.playerName = manager.UserName;
		move (2);


	}

	public void OnConnectedToMaster() {
		JoinLobby ();
	}

	public void Singleplayer () {
		SceneManager.LoadScene ("test");
	}


	public void Exit() {
		Application.Quit ();
	}

	public void JoinLobby () {
		PhotonNetwork.JoinLobby ();
	}
		

	public void move (int num) {
		_state = num;
	}

	void Update () {

		if (_state == 1) {
			_camera.transform.position = Vector3.Lerp (_camera.transform.position, new Vector3 (27, 10, 0), 0.1f); /// to multiplayergame
		} 
		if (_state == 2) {
			_camera.transform.position = Vector3.Lerp (_camera.transform.position, new Vector3 (0, 7, -11), 0.1f); /// to main menu
		}
		if (_state == 3) {
			_camera.transform.position = Vector3.Lerp (_camera.transform.position, new Vector3 (-10, 32, 39), 0.1f); /// to create room
		}
		if (_state == 4) {
			_camera.transform.position = Vector3.Lerp (_camera.transform.position, new Vector3 (53, 13, 10), 0.1f); /// to join room
		}
	}

	public void CreateRoom (GameObject roomname) {

		Hashtable roomProps = new Hashtable () { {"name", roomname.GetComponent<Text> ().text} };
		string[] RoomPropsInLobby = new string[] { "name" };


		RoomOptions _roomop = new RoomOptions () { isVisible = true, isOpen = true, maxPlayers = 20, customRoomProperties = roomProps, customRoomPropertiesForLobby = RoomPropsInLobby };
		PhotonNetwork.CreateRoom (roomname.GetComponent<Text> ().text, _roomop, TypedLobby.Default);

		roomname.GetComponent<Text> ().text = "";


	}

	public void OnCreatedRoom () {
		Debug.Log ("It works!!!");
		foreach (RoomInfo game in PhotonNetwork.GetRoomList ()) {
			Debug.Log (game.customProperties.Values);
		}
	}

	public void OnPhotonCreateGameFailed () {
		Debug.Log ("Ooooops!");
	}

	public void OnPhotonJoinRoomFailed () {
		Debug.Log ("Ooops!");
	}

	public void Join () {
		//Hashtable roomProps = new Hashtable () { {"name", roomname.GetComponent<Text> ().text} };
		//string[] RoomPropsInLobby = new string[] { "name" };
		int k;
		string map;
		map = mapselector.GetComponent<Text> ().text;
		k = 0;
		switch (map) {
		case "free1": 
			k = 2;
			break;
		case "free2": 
			k = 3;
			break;
		case "ctf": 
			k = 4;
			break;
		}


		RoomOptions _roomop = new RoomOptions () { isVisible = true, isOpen = true, maxPlayers = 10, /*customRoomProperties = roomProps, customRoomPropertiesForLobby = RoomPropsInLobby*/ };

		PhotonNetwork.JoinOrCreateRoom ("Server" + k, _roomop, TypedLobby.Default);
	}

	public void OnJoinedRoom () {
		int k;
		string map;
		map = mapselector.GetComponent<Text> ().text;
		k = 0;
		manager.type = GameType.Free;
		GameType type = GameType.Free;

		switch (map) {
		case "free1": 
			k = 2;

			break;
		case "free2": 
			k = 3;
			break;
		case "ctf": 
			k = 4;
			type = GameType.CaptureTheFlag;
			break;
		}


		if (type == GameType.CaptureTheFlag) {
			int bt = 0;
			int rt = 0;
			manager.type = GameType.CaptureTheFlag;

			foreach (PhotonPlayer player in PhotonNetwork.otherPlayers) {
				if (player.GetTeam () == PunTeams.Team.blue) {
					bt += 1;
				} else {
					rt += 1;
				}
			}

			if (rt >= bt) {
				PhotonNetwork.player.SetTeam (PunTeams.Team.blue);
			} else {
				PhotonNetwork.player.SetTeam (PunTeams.Team.red);
			}
			PhotonNetwork.player.SetCustomProperties (new ExitGames.Client.Photon.Hashtable(){{"hasFlag", false}});
		}


		PhotonNetwork.LoadLevel (k);
	}

	//public void

}
