﻿using UnityEngine;
using System.Collections;

public class NetworkBullet : MonoBehaviour {

	public float timer = 10.0f;

	// Use this for initialization
	void Start () {
		timer = 10;
	}
	
	// Update is called once per frame
	void Update () {
		if (timer <= 0) {
			PhotonNetwork.Destroy (this.GetComponent<PhotonView> ());
		}

		timer = timer - Time.deltaTime;

		//Debug.Log (timer);

	}
}
