﻿using UnityEngine;
using System.Collections;

public class Instantiator : Photon.MonoBehaviour {


	public void OnLevelWasLoaded () {
		GameObject SpawnPoint = GameObject.Find ("SpawnPoint");

		if (PhotonNetwork.player.GetTeam () == PunTeams.Team.blue) {
			SpawnPoint = GameObject.Find ("SpawnPoint_blue");
		}

		if (PhotonNetwork.player.GetTeam () == PunTeams.Team.red) {
			SpawnPoint = GameObject.Find ("SpawnPoint_red");
		}

		PhotonNetwork.Instantiate ("player", SpawnPoint.transform.position, SpawnPoint.transform.rotation, 0);
	}
}
