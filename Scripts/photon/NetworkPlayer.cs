﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class NetworkPlayer : Photon.MonoBehaviour {

	public GameObject MyCamera;

	private GameManager manager;

	public Text name;

	void Start () {

		manager = GameObject.Find ("_gameManager").GetComponent<GameManager> ();
		name.text = manager.UserName;
		if (photonView.isMine) {
			
			MyCamera.SetActive (true);
			this.GetComponent<FirstPersonController> ().enabled = true;
			this.GetComponent<GravityBody> ().enabled = true;
		} else {
			MyCamera.SetActive (false);
			this.GetComponent<FirstPersonController> ().enabled = false;
			this.GetComponent<GravityBody> ().enabled = false;
		}

		name.text = this.GetComponent<PhotonView> ().owner.name;
		
	}
		

}
