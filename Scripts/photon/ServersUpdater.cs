﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ServersUpdater : MonoBehaviour {

	public Dropdown _dropdown;


	void Update () {
		_dropdown.options = new System.Collections.Generic.List<Dropdown.OptionData>();

		//for (int i = 0;  i < PhotonNetwork.GetRoomList().Length; i++) {
		foreach (RoomInfo game in PhotonNetwork.GetRoomList ()){
			_dropdown.options.Add(new Dropdown.OptionData(game.name));
		}
	}
}
