﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


public class tab_team_script : MonoBehaviour {

	public Text players_red;
	public Text players_blue;
	public Text score_red;
	public Text score_blue;




	void Start () {

	}

	void Update () {

		int scoreb = 0;
		int scorer = 0;




		players_red.text = "";
		players_blue.text = "";

		PhotonPlayer lplayer;

		lplayer = PhotonNetwork.player;

		if (lplayer.GetTeam () == PunTeams.Team.blue) {
			players_blue.text = players_blue.text + lplayer.name +"   "+ lplayer.GetScore() + "\n";
			scoreb += lplayer.GetScore ();
		}

		if (lplayer.GetTeam () == PunTeams.Team.red) {
			players_red.text = players_red.text + lplayer.name +"   "+ lplayer.GetScore() + "\n";
			scorer += lplayer.GetScore ();
		}


		foreach (PhotonPlayer player in PhotonNetwork.otherPlayers) {

			if (player.GetTeam () == PunTeams.Team.blue) {
				players_blue.text = players_blue.text + player.name +"   "+ player.GetScore() + "\n";
				scoreb += player.GetScore ();
			}

			if (player.GetTeam () == PunTeams.Team.red) {
				players_red.text = players_red.text + player.name +"   "+ player.GetScore() + "\n";
				scorer += player.GetScore ();
			}
		
		}
		score_blue.text = "" + scoreb;
		score_red.text = "" + scorer;


	}
}
